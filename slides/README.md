---
title: CLI
verticalSeparator: ^----$
---

# CLI

note:
    accronyme ?

----

- **C**ommand
- **L**ine
- **I**nterface

note:
    terminal : VSCode + Konsole

---

## Historique

Héritage

- années 70 : Unix
  - 1984 : projet GNU <!-- .element: class="fragment" -->
- pas de GUI <!-- .element: class="fragment" -->
- écran cathodique
  - **monochrome**
    - vert <!-- .element: class="fragment" -->
- performance / octet couteux
  - légèreté < UX <!-- .element: class="fragment" -->
- ... <!-- .element: class="fragment" -->

---

## Objectifs

- <span style="display: inline-block; color: lawngreen; background-color: black; font-family: 'Courier New', monospace; padding: 0.5em 1em;">$ S'éloigner du cliché du pavé de texte vert sur fond noir</span>
  - (_dégueulasse_ 🤮) <!-- .element: class="fragment" -->
- Montrer que ça __peut__ (et _devrait_) __etre facile et agréable__ à utiliser <!-- .element: class="fragment" -->
- __Inciter__ à utiliser les outils en CLI <!-- .element: class="fragment" -->
- ... <!-- .element: class="fragment" -->

note:
    plein d'outils, à vous de notez les outils qui vous intéresse

---

## Des outils pratiques

----

[ngrok](https://ngrok.com) 💻➡☁️ expose un serveur HTTP local sur internet

```sh
ngrok http 8080
```

```txt
ngrok                                                                          (Ctrl+C to quit)
                                                                                               
Try our new native Go library: https://github.com/ngrok/ngrok-go                               
                                                                                               
Session Status                online                                                           
Session Expires               1 hour, 59 minutes                                               
Update                        update available (version 3.1.1-rc1, Ctrl-U to update)           
Terms of Service              https://ngrok.com/tos                                            
Version                       3.0.4                                                            
Region                        Europe (eu)                                                      
Latency                       14ms                                                             
Web Interface                 http://127.0.0.1:4040                                            
Forwarding                    https://dd0b-109-7-227-228.eu.ngrok.io -> http://localhost:8080  
                                                                                               
Connections                   ttl     opn     rt1     rt5     p50     p90                      
                              0       0       0.00    0.00    0.00    0.00       
```

----

[ngrok](https://ngrok.com) 💻➡☁️ expose un serveur HTTP local sur internet

_pas open source_

gratuit pour :

- 1 port
- pour un usage non commercial
- session de 2h

----

[tmate](https://tmate.io) 💻➡☁️ partage un terminal sur internet via SSH

```sh
tmate
```

```txt
Tip: if you wish to use tmate only for remote access, run: tmate -F          [0/0]
To see the following messages again, run in a tmate session: tmate show-messages
Press <q> or <ctrl-c> to continue
---------------------------------------------------------------------
Connecting to ssh.tmate.io...
Note: clear your terminal before sharing readonly access
web session read only: https://tmate.io/t/ro-3G9X8ufyGNjVXJKyZmvxVzJqP
ssh session read only: ssh ro-3G9X8ufyGNjVXJKyZmvxVzJqP@lon1.tmate.io
web session: https://tmate.io/t/6WHmpC7P8txGAY3XnSLS4dHek
ssh session: ssh 6WHmpC7P8txGAY3XnSLS4dHek@lon1.tmate.io

[0] 0:[tmux]*                              "pinage404-sabre15-nix" 00:42 31-janv.-
```

----

[jq](https://stedolan.github.io/jq/) `[{}]`➡`{}` *J*SON *Q*uery

```sh
echo '[
    {"name":"JSON", "good":true},
    {"name":"XML", "good":false}
]' | jq '[.[].name]'
```

```json
[
  "JSON",
  "XML"
]
```

----

[neofetch](https://github.com/dylanaraps/neofetch) 🖼️ vue d'ensemble d'un système d'exploitation

![](https://camo.githubusercontent.com/857a5a0ccfa464dfcfd195902677aa3cb87a1f430a5f8a49574715c3263b72be/68747470733a2f2f692e696d6775722e636f6d2f6c55726b51424e2e706e67)

----

[onefetch](https://github.com/o2sh/onefetch) 🖼️ vue d'ensemble d'un projet

![](https://github.com/o2sh/onefetch/raw/b6bb81e30275539ef0cf7da334985f70a8191527/assets/screenshot-2.png)

----

[tokei](https://github.com/XAMPPRocky/tokei) 📊 statistiques de fichiers sources par languages

```sh
tokei
```

```txt
===============================================================================
 Language            Files        Lines         Code     Comments       Blanks
===============================================================================
 Nix                     1           47           46            0            1
-------------------------------------------------------------------------------
 Markdown                2          568            0          358          210
 |- JSON                 1            4            4            0            0
 (Total)                            572            4          358          210
===============================================================================
 Total                   3          615           46          358          211
===============================================================================
```

----

[kondo](https://github.com/tbillington/kondo) 🚮 supprime les dossiers non nécessaires

`target`
`node_modules`
`.venv`
👀

![](https://user-images.githubusercontent.com/2771466/89015432-5c765e00-d35a-11ea-8e67-193f2688d660.png)

----

[imagemagick](https://imagemagick.org) 🖼️ éditeur d'image

<div class="fragment">

Convertion

```sh
magick 'toto.png' 'toto.jpg'
             ^^^        ^^^
```

</div>

<div class="fragment">

Redimentionnement

```sh
magick 'toto.jpg' -resize 120x120 'toto.jpg'
```

</div>

---

## GNU core utilities

### Alternatives Modernes

note:
    Linux / Mac coreutils

----

[exa](https://the.exa.website/) 📂 lister le contenu d'un dossier

comme GNU `ls` + GNU `tree` mais :

- `+` facile
  - options explicites
- Git integration
- couleurs

----

[exa](https://the.exa.website/) 📂 lister le contenu d'un dossier

![](exa.png)

----

[fd](https://github.com/sharkdp/fd) 🔎 chercher _des fichiers_

comme GNU `find` mais :

- `+` facile
  - options explicites
  - regexp
- ⚡ rapide
  - respecte le `.gitignore`

----

[fd](https://github.com/sharkdp/fd) 🔎 chercher _des fichiers_

![](https://github.com/sharkdp/fd/raw/af9daff4eea5162c9a4fd5e7ab4e10f9b784310e/doc/screencast.svg)

----

[ripgrep](https://github.com/BurntSushi/ripgrep) 🔍 chercher _**dans** des fichiers_

comme GNU `grep` mais :

- `+` facile
  - options explicites
  - regexp
- ⚡ rapide
  - respecte le `.gitignore`

----

[ripgrep](https://github.com/BurntSushi/ripgrep) 🔍 chercher _**dans** des fichiers_

![](https://camo.githubusercontent.com/fe9229337cbfaffcf22d52a1e363f268a3c68901acc8f66116bf8705655e92b0/68747470733a2f2f6275726e7473757368692e6e65742f73747566662f72697067726570312e706e67)

----

[bat](https://github.com/sharkdp/bat) 📄 afficher un fichier

comme GNU `cat` + `nl` mais :

- syntax highlight
- Git integration

----

[bat](https://github.com/sharkdp/bat) 📄 afficher un fichier

![](https://camo.githubusercontent.com/c436c206f2c86605ab2f9fb632dd485afc05fccbf14af472770b0c59d876c9cc/68747470733a2f2f692e696d6775722e636f6d2f326c53573452452e706e67)

----

[mdcat](https://github.com/swsnr/mdcat) 📄 afficher un fichier _Markdown_

comme GNU `cat` mais :

- syntax highlight
- render Markdown

![](https://github.com/swsnr/mdcat/raw/931b294ca5f1515e07cc99cd4242e15b29d14f03/screenshots/side-by-side.png)

----

[ncdu](https://dev.yorhel.nl/ncdu) 📊 trouver les dossiers lourds

comme GNU `du` mais :

- graphique
- tri
  - critères
- suppresion de fichiers
- raccourcis clavier
- couleurs

----

[ncdu](https://dev.yorhel.nl/ncdu) 📊 trouver les dossiers lourds

![](https://dev.yorhel.nl/img/ncduconfirm-2.png)

----

[watchexec](https://watchexec.github.io/) 🔄 répéter une commande

ressemble à GNU `watch` mais :

- s'éxécute dès qu'un fichier change

```sh
watchexec make
```

----

[sd](https://github.com/chmln/sd) ﯒ remplacer

ressemble à GNU `sed` mais :

- regexp
- chercher et remplacer _uniquement_

```sh
cat fichier | sd before after
sd before after fichier
```

----

[htop](https://htop.dev/) gérer les processus

ressemble à GNU `top` mais :

- facile
  - raccourcis claviers _affichés_
- couleurs
- cliquable
- graph

----

[htop](https://htop.dev/) gérer les processus

![](https://htop.dev/images/htop_graph.gif)

----

[btop](https://github.com/aristocratos/btop) 📊 graphique système

ressemble à GNU `top` mais :

- **graph**
- facile
  - raccourcis claviers _affichés_
- couleurs
- cliquable

----

[btop](https://github.com/aristocratos/btop) 📊 graphique système

![](https://github.com/aristocratos/btop/blob/c4ee41ebc0f7fabbf4717450c889292464dbeb6d/Img/normal.png?raw=true)

----

[ctop](https://ctop.sh/) 📊 trouver les containers gourmands

ressemble à GNU `top` mais :

- pour les containers

![](https://ctop.sh/img/screencap.gif)

---

~~Bash / ZSH~~

## [Fish](https://fishshell.com/)

![](fish_logo.png)

note:
    shell alternatif

----

Autocompletion des arguments

![](https://fishshell.com/assets/img/screenshots/works_out_of_the_box.png)

automatiquement généré

----

Autocompletion des options

![](20230129013215.png)

automatiquement générée pour les logiciels installés

----

Autosuggestion

![](https://fishshell.com/assets/img/screenshots/autosuggestion.png)

basée sur l'historique

----

[Oh My Fish](https://github.com/oh-my-fish/oh-my-fish)

<img src="https://camo.githubusercontent.com/d08525bf52fccda31b66635abd536e52f74f5ee79c3ee96b788d8a13d57980c7/68747470733a2f2f63646e2e7261776769742e636f6d2f6f682d6d792d666973682f6f682d6d792d666973682f653466316332653032313961313765326337343862383234303034633864306233383035356331362f646f63732f6c6f676f2e737667" data-canonical-src="https://cdn.rawgit.com/oh-my-fish/oh-my-fish/e4f1c2e0219a17e2c748b824004c8d0b38055c16/docs/logo.svg" style="max-width: 100%;" width="192px" height="192px">

note:
    gestionnaire de paquets pour Fish

----

[enlarge_your_git_alias](https://gitlab.com/pinage404/omf_pkg_enlarge_your_git_alias)

<script id="asciicast-496261" src="https://asciinema.org/a/496261.js" async></script>

----

[gityaw](https://github.com/oh-my-fish/plugin-gityaw)

```sh
git clone https://gitlab.com/pinage404/pinage404-vscode-extension-packs
```

🕜

```sh
git push
```

<div class="fragment">

🤦 HTTPS

</div>

<div class="fragment">

```sh
gityaw
Processing remote origin...
Replaced 'https://gitlab.com/pinage404/pinage404-vscode-extension-packs' by 'git@gitlab.com:pinage404/pinage404-vscode-extension-packs.git'
git push
```

🙂 SSH 🔒

</div>

----

[sponge](https://github.com/meaningful-ooo/sponge.git)

----

[insist](https://gitlab.com/lusiadas/insist)

----

[done](https://github.com/franciscolourenco/done)

----

[autopair](https://github.com/jorgebucaran/autopair.fish)

`([{}])`

----

[sudope](https://github.com/oh-my-fish/plugin-sudope)

```sh
fdisk --list
```

<div class="fragment">

```txt
fdisk: impossible d'ouvrir /dev/sda: Permission denied
fdisk: impossible d'ouvrir /dev/sdb: Permission denied
```

😠

</div>

<div class="fragment">

[[alt]] + [[S]]

</div>

<div class="fragment">

```sh
sudo fdisk --list
```

🙂

</div>

----

[des thèmes](https://github.com/oh-my-fish/oh-my-fish/blob/master/docs/Themes.md) pour customiser son prompt

[bobthefish](https://github.com/oh-my-fish/oh-my-fish/blob/master/docs/Themes.md#bobthefish-1)

![](https://cloud.githubusercontent.com/assets/53660/18028510/f16f6b2c-6c35-11e6-8eb9-9f23ea3cce2e.gif)

---

## Prompt / Invite de commande

----

[![starship](https://starship.rs/logo.svg)](https://starship.rs/)

note:
    prompt alternatif
    cross shell
    super customizable

----

<video controls>
<source src="https://starship.rs/demo.webm" type="video/webm" />
<source src="https://starship.rs/demo.mp4" type="video/mp4" />
</video>

----

[![](https://starship.rs/presets/img/pastel-powerline.png)](https://starship.rs/presets/pastel-powerline.html)

---

## Font / Police de caractères

----

[Powerline](https://github.com/powerline/fonts)

![](https://camo.githubusercontent.com/b0862287feb56d682f22cfa67bc43f5cfa2fd5b59fc291533be741015c6766af/68747470733a2f2f7261772e6769746875622e636f6d2f622d7279616e2f706f7765726c696e652d7368656c6c2f6d61737465722f626173682d706f7765726c696e652d73637265656e73686f742e706e67)

----

[Nerd Fonts](https://www.nerdfonts.com/)

Powerline

![](https://www.nerdfonts.com/assets/img/nerd-fonts-powerline-extra-terminal.png)

----

[Nerd Fonts](https://www.nerdfonts.com/)

Icones

![](https://www.nerdfonts.com/assets/img/nerd-fonts-icons-in-vim.png)

----

[Nerd Fonts](https://www.nerdfonts.com/)

![](https://www.nerdfonts.com/assets/img/sankey-glyphs-combined-diagram.png)

----

[FiraCode](https://github.com/tonsky/FiraCode)

![](https://github.com/tonsky/FiraCode/blob/63976dd1297dfa2f3e3024889aa865cb34b16c54/extras/samples.png?raw=true)

----

[FiraCode](https://github.com/tonsky/FiraCode)

![](https://github.com/tonsky/FiraCode/blob/63976dd1297dfa2f3e3024889aa865cb34b16c54/extras/samples2.png?raw=true)

----

[FiraCode](https://github.com/tonsky/FiraCode)

![](https://github.com/tonsky/FiraCode/blob/63976dd1297dfa2f3e3024889aa865cb34b16c54/extras/ligatures.png?raw=true)

---

Questions ?

<!-- markdownlint-disable -->
<p class="legal">
    <a rel="license" href="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">
        <img alt="Licence Creative Commons BY-NC-SA" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" />
    </a>
    by
    <a rel="author external" href="https://wheretofind.me/@pinage404">
        <img alt="pinage404" src="https://s.gravatar.com/avatar/0a7d96df27d5d020cb0d03340e734180?s=40" />
    </a>
    on
    <a rel="alternate" href="https://gitlab.com/pinage404/cli_ftw">
        <img alt="GitLab" src="https://img.shields.io/gitlab/stars/pinage404/cli_ftw?style=social" />
    </a>
</p>
<style>
.legal img {
    margin: 0;
    vertical-align: middle;
}
</style>
