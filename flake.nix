{
  inputs.nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
  inputs.flake-utils.url = "github:numtide/flake-utils";

  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = nixpkgs.legacyPackages."${system}";
        tools = with pkgs; [
          tmate
          jq
          neofetch
          onefetch
          tokei
          kondo
          imagemagick
        ];
        better_gnu = with pkgs; [
          exa
          fd
          ripgrep
          bat
          mdcat
          ncdu
          watchexec
          sd
          htop
          btop
          ctop
        ];
        shell = with pkgs; [
          bashInteractive
          fish
        ];
        prompt = with pkgs; [
          starship
        ];
      in
      {
        devShell = pkgs.mkShell {
          packages = [
            pkgs.git
          ]
          ++ tools
          ++ better_gnu
          ++ shell
          ++ prompt
          ;
        };
      });
}
